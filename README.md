Guide for Collaboration. This is for github, but same principles apply.
https://www.digitalocean.com/community/tutorials/how-to-create-a-pull-request-on-github


<h3>To Install:</h3><br/>
<code>
pip install pipenv  # Make sure you have pipenv installed
git clone https://gitlab.com/dfitzpatrick/ZC4Bot.git
cd ZC4Bot
pipenv install
</code>


<h3>Environment Variables</h3><br/>
You will need to set up the following environment variables to your account.
<ul>
<li>BOT_TOKEN   # Your bot api token from the discord developer pages.</li>
<li>API_ROOT_URL    # usually localhost:8000/api/ if you are running the django server locally.</li>
</ul>

<h3>To run the bot:</h3><br/>
<code>
python -m bot
</code>
