import logging
from discord.ext import commands
from discord.ext.commands import Context
import discord
from discord.utils import get
log = logging.getLogger(__name__)


def with_role(role_names: tuple):
    """Decorator to check if the invoking discord user has the role associated
    to run the command.
    """
    async def predicate(ctx: Context):
        if not ctx.guild:  # Return False in a DM
            return False

        author_roles_name = ', '.join(r.name for r in ctx.author.roles)
        r_names = [r.lower() for r in role_names]
        for role in ctx.author.roles:
            if role.name.lower() in r_names:
                return True
        return False
    return commands.check(predicate)

