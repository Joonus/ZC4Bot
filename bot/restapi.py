import json
import logging
import os
from http import HTTPStatus
from typing import Optional, Tuple, Dict, Any, Union

import aiohttp
from discord.ext import commands

API_ROOT_URL = os.environ.get("API_ROOT_URL")
if not API_ROOT_URL:
    raise Exception("API_ROOT_URL environment variable undefined.")
log = logging.getLogger(__name__)


class APIObjectDoesNotExist(commands.BadArgument):
    """Custom exception for use in converters"""
    pass


def build_target(endpoint: str, resource_id: Optional[int] = None) -> str:
    target = f"{API_ROOT_URL}{endpoint}/"
    target = target if resource_id is None else f"{target}{resource_id}/"
    return target


def auth_header() -> dict:
    auth_token = os.environ.get('API_AUTH_TOKEN')
    result = {'Authorization': f'Token {auth_token}'} if auth_token else None
    return result

def flatten_relationships(api_object: Dict[str, Any], key='id') -> Dict[str, Any]:
    def get_key(o: Union[Dict[str, Any], str]) -> Any:
        if isinstance(o, dict):
            return o.get(key)
        return o

    modified = api_object.copy()
    for obj_key, item in modified.items():
        if isinstance(item, dict):
            item = item.get(key)
        elif isinstance(item, list):
            container = [get_key(o) for o in item]
            item = container
        modified[obj_key] = item
    return modified

#TODO: Refactor prune()
def prune(d: Dict[str, Any]):
    """Modifies inplace"""
    iter_dict = d.copy()
    for k in iter_dict.keys():
        if d[k] is None:
            del d[k]
        elif isinstance(d[k], dict):
            prune(d[k])
    return d


async def api_method(method: str,
                     target: str,
                     *,
                     session: Optional[aiohttp.ClientSession] = None,
                     **kwargs) -> Tuple[str,  Optional[Dict[str, Any]]]:
    async def execute() -> Tuple[str, Optional[Dict[str, Any]]]:
        log.debug(f"CALLING API METHOD '{method}' on {target} -- {kwargs}")
        resp = await session.__getattribute__(method)(target, **kwargs)
        status = resp.status
        result = await resp.text()
        debug_result = f"- {result}" if not os.environ.get('SUPPRESS_API_RESULT', True) else ''
        log.debug(f"API METHOD '{method}' returns {status} {debug_result}")
        try:
            return status, json.loads(result)
        except json.decoder.JSONDecodeError as e:
            log.error(e.msg)
            return status, None
    if session is None:
        async with aiohttp.ClientSession(headers=auth_header()) as session:
            return await execute()
    else:
        return await execute()


async def get(endpoint: str,
              resource_id: Optional[int] = None,
              session: Optional[aiohttp.ClientSession] = None,
              ) -> Optional[Dict[str, Any]]:

    target = build_target(endpoint, resource_id)
    status, result = await api_method('get', target,
                                      session=session)
    if status == HTTPStatus.OK:
        return result


async def save(endpoint: str,
               instance: Dict[str, Any], *,
               id_key=None,
               nested_keys_only: bool = False,
               session: Optional[aiohttp.ClientSession] = None,
               silent: bool = True,
               partial: bool = False) -> Optional[Dict[str, Any]]:
    resource_id = instance.get(id_key) if id_key else instance.get('id')
    method = 'post' if resource_id is None else 'patch' if partial else 'put'
    target = build_target(endpoint, resource_id=resource_id)

    if nested_keys_only:
        instance = flatten_relationships(instance)
    prune(instance)

    status, result = await api_method(method, target,
                                      json=instance, session=session)
    if status == HTTPStatus.CREATED or  status == HTTPStatus.OK:
        return result
    else:
        if not silent:
            raise APIObjectDoesNotExist(f"{status} - {result}")
