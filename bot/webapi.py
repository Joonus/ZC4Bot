import logging
import aiohttp
import json
from datetime import datetime
import os
from http import HTTPStatus
from discord.ext import commands
from dateutil import tz, parser
from bot import constants
from copy import deepcopy
from bot import utils
log = logging.getLogger(__name__)

# This bot now uses environment variables. In Heroku, these are COnfig Vars.
# Set this API_ROOT_URL to either the main web site, or a local instance.
API_ROOT_URL = os.environ['API_ROOT_URL']

async def api_base_method(method: str,
                          endpoint: str,
                          resource_id = "",
                          session=None,
                          **kwargs) -> aiohttp.ClientResponse:
    """Generic function that takes a method for a web api call.
    Valid are GET/PUT/POST

    endpoint is the full url path.
    """
    url = endpoint if resource_id == "" else f"{endpoint}{resource_id}/"
    log.info(f"Calling api_method: {method} on {url}. ")
    for key in kwargs:
        if key == 'json' or key == 'data':
            log.info(f"api_method {key.upper()}: {kwargs.get(key)}")

    auth_token = os.environ.get('API_AUTH_TOKEN', None)
    auth_header = {'Authorization': f'Token {auth_token}'} if auth_token else None


    if not session:
        session = aiohttp.ClientSession(headers=auth_header)
        response = await session.__getattribute__(method)(url, **kwargs)
        response_status = response.status
        response_text = await response.text()
        await session.close()
    else:
        response = await session.__getattribute__(method)(url, **kwargs)
        response_status = response.status
        response_text = await response.text()


    log.info(f"api_method {method} returns {response_status} - "
             f"{response_text}")
    return response_status, response_text


class APIObjectDoesNotExist(commands.BadArgument):
    """Custom exception for use in converters"""
    pass





class WebAPIMixin:
    """Mixin designed to give COGS access to our web api functionality without
    having to worry too much about boiler plate."""
    def __init__(self):
        super(WebAPIMixin, self).__init__()

    async def api_save(self, endpoint, data: dict, ids_only=False) -> dict:
        """Persists a 'data' object into our web api interface.

        :param endpoint The endpoint to point to ex: `match/`
        :param data     The information needed to properly POST.
        :param ids_only True/False if m2m objects should only be the pk.
        """


        log.debug(f'IN API SAVE: Data is {data}')
        if data.get('id', None):
            method = 'put'
            resource_id = data['id']
        else:
            method = 'post'
            resource_id = ""
        endpoint = f"{self.api_root}{endpoint}/"


        if ids_only:
            # Support DRF multiple serializers, (one for viewing nested,
            # and one for writing to just ids. DRF is a pain when it comes
            # to nested writable serializers.
            # TODO: Explore other solutions to DRF Writable Nested.
            for key in data:
                if type(data[key]) == dict:
                    log.debug(f"{key} is dict. Flattening {data[key]}")
                    id = data[key].get('id', None)
                    data[key] = id
                elif type(data[key]) == list:
                    # many to many
                    log.debug(f"{key} is list. Flattening {data[key]}")
                    result = []
                    for o in data[key]:
                        if type(o) == dict:
                            result.append(o['id'])
                        else:
                            # Not an object. add it to the list.
                            result.append(o)

                    data[key] = result
        # Remove any keys that are None
        data = utils.remove_keys_as_none(data)


        status, resp = await self.api_method(method=method,
                                             resource_id=resource_id,
                                             endpoint=endpoint,
                                             json=data,
                                             )
        if status == HTTPStatus.OK or status == HTTPStatus.CREATED:
            return json.loads(resp)
        else:
            return None
        #return status == HTTPStatus.OK if method == 'put' else HTTPStatus.CREATED

    def to_datetime(self, date_string: str) -> datetime:
        """Parses into a python date time object.
        :param date_string  The string that associates a date/time.

        This will try to parse many different string representations of datetime
        into a python object.
        """
        return parser.parse(date_string)

    def normalize_date(self, date_string: str,
                       to_timezone: str = constants.TIMEZONE) -> str:
        """Normalizes a datetime object into a human readable string.
        :param date_string  The string that associates a date/time.
        :param to_timezone  A valid Timezone to convert to. ex: `US/Central`
        :return str         A human readable string representation of a datetime
        """
        return self.to_datetime(date_string).astimezone(
            tz.gettz(to_timezone)).strftime('%D - %I:%M%p')

    async def get_record(self, endpoint: str, id: int, **kwargs):
        """Returns a web api object from our database.
        :param endpoint     The endpoint to target. ex: `match/`
        :param id           The pk of the object from our web api.
        :return dict        A dict representation of the object.
        """
        endpoint = f"{self.api_root}{endpoint}/"
        status, record = await api_base_method(method='get',
                                               endpoint=endpoint,
                                               resource_id=id,
                                               **kwargs)
        if status == 200:
            return json.loads(record)


    async def all_records(self, endpoint: str, **kwargs):
        """Returns a list of all web api objects from our database.
        :param endpoint     The endpoint to target. ex: `match/`
        :return List[dict]  A python list of dicts that represent the objects.
        """
        params = ""
        for i, param in enumerate(kwargs):
            sep = "?" if i == 0 else "&"
            params = params + f"{sep}{param}={kwargs[param]}"

        endpoint = f"{self.api_root}{endpoint}/{params}"
        status, record = await api_base_method(method='get',
                                               endpoint=endpoint)
        if status == 200:
            return json.loads(record)



    @property
    def api_root(self) -> str:
        """API ROOT as defined."""
        # TODO: Add Configuration Setting
        return API_ROOT_URL

    @property
    def api_endpoint(self) -> str:
        """Returns the fully qualified api_endpoint path."""
        try:
            return f"{self.api_root}{self.endpoint}"
        except AttributeError as error:
            log.error(f"endpoint not defined in {self.__class__} - {error}")
            raise


    async def api_method(self, method: str,
                         resource_id: str = "",
                         endpoint: str = "",
                         **kwargs) -> aiohttp.ClientResponse:
        """Generic function for web api calls.
        :param method       The HTTP GET/PUT/POST method.
        :param resource_id  The pk of the endpoint. ex `1` points to /match/1/
        :param endpoint     The endpoint to target. ex: `match/`
        :return tuple       Tuple contains (status, response)
        """
        if endpoint == "":
            endpoint = self.api_endpoint
        session = constants.BOT.http_session
        status, response = await api_base_method(method=method,
                                                 endpoint=endpoint,
                                                 resource_id=resource_id,
                                                 session=session,
                                                 **kwargs)
        return status, response
