import asyncio
import collections
import datetime
import json
import logging
import os
import textwrap
import typing

import dateutil.parser
import discord
import websockets
from discord.ext import tasks

from bot import utils
from bot.constants import BOT as client
from bot.constants import EMOJIS, TWITCH_BOT

log = logging.getLogger(__name__)

def safe_value(value, default_attr='id', type_req=int) -> typing.Optional[typing.Any]:
    """Helper function to either use a object or an native type to get a value

    Parameters
    ----------
    value: Any
        The native value (or object) to check.
    default_attr: str
        The attribute to use to retrieve the value (if any).
    type_req: type
        The type of value that is expected.
    Returns
    -------
    Optional[Any]
    """
    if not hasattr(value, default_attr) and not isinstance(value, type_req):
        # log.error(f"Invalid value sent: {value}")
        return
    value = value if not hasattr(value, default_attr) else getattr(value, default_attr)
    return value

class HostSession:
    """
    This acts as the main container, or context of a given session.

    Hosts are those that queue up a dashboard on the discord channel to organize
    games. This session data tracks those that have checked into the games and
    helps facilitate helpful functions such as player prioritizing on checkins
    and tracking matches to make announcements to the server.

    """

    def _safe_set(self, attr, value, *, default=None, default_attr='id', type_req=int):
        """Safely set a value.

        This is used for our properties of stateful discord objects. The idea
        behind the class is to allow it to serialize much easier. We still use
        dill for now, but the idea would be to move to json when ready.

        Parameters
        ----------
        attr: str
            The attribute of a HostSession instance to set
        value: Any
            The value to set the attribute.
        default:
            The default value in case of a non-truthy value.
        default_attr
            The default attribute to use for a value.
        type_req: type
            The type of the value that is required.

        Returns
        -------
        None
        """
        if isinstance(value, list) and not isinstance(value, str):
            value = [safe_value(v, default_attr=default_attr, type_req=type_req) for v in value]
        else:
            value = safe_value(value)
        setattr(self, attr, value or default)

    def __init__(self, guild_id: int, originator: int, hosts: typing.List[int]):
        """Creates a new HostSession

        This holds the stateful information for any session or dashboard.

        Because discord objects are stateful, we store their ids and use
        properties to fetch the actual discord objects.

        The only limits to this that complicates things are messages which are
        async in nature. See docstrings on those particular properties below for
        special handling.

        Parameters
        ----------
        guild_id: int
            The guild id that the session exists on.
        host_ids: List[int]
            A Sequence of discord.Member ids that will act as the hosts.
        """
        self._guild: int = guild_id
        self._hosts: typing.List[int] = hosts
        self._originator: typing.Optional[int] = originator

        self._channel: typing.Optional[int] = None
        self._dashboard: typing.Optional[int] = None
        self._unlinked_embed: typing.Optional[int] = None
        self._checkins: typing.List[int] = []
        self._streamer: typing.Optional[int] = None
        self._rolled_in: typing.List[int] = []
        self._rolled_out: typing.List[int] = []
        self.socket: typing.Optional[websockets.WebSocketClientProtocol] = None
        self._required_roles: typing.List[int] = []
        self._category: typing.Optional[discord.CategoryChannel] = None
        self._team_channels: typing.List[discord.VoiceChannel] = []
        self._lobby_channel: typing.Optional[discord.VoiceChannel] = None
        self._streamer_channel: typing.Optional[discord.VoiceChannel] = None
        self.twitch_stream: typing.Optional[str] = 'zcleagues'

        self.unlinked_profiles: typing.List[typing.Dict[str, typing.Any]] = []
        self.winners_advance = True
        self.matches = {}
        self._last_activity = utils.utc_now().isoformat()
        self._announcement: typing.Optional[int] = None
        self._announcement_channel: typing.Optional[int] = None
        self._announcement_role: typing.Optional[int] = None
        self._host_role: typing.Optional[int] = None
        self._pinged_roles = False
        self._command_invoked: typing.Optional[discord.ext.commands.Command] = None

        # Temporary fix
        self.dashboard_task: typing.Optional[asyncio.Task] = None
        self.consumer_task: typing.Optional[asyncio.Task] = None


        # to re-implement with Tasks in 1.3
        # self.start_consumer_task()
        # self.start_dashboard_task()


    def __del__(self):
        log.warning("In HostSession __del__")
        self.stop_tasks()

        # self.websocket_consumer.cancel()
        # self.dashboard_channel.cancel()

    def stop_tasks(self):
        if self.consumer_task:
            self.consumer_task.cancel()
        if self.dashboard_task:
            self.dashboard_task.cancel()

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __getstate__(self):
        """
        Helps with serializing. Some types cannot be serialized but must be
        tracked, much to no avail.

        Returns
        -------
        None
        """
        obj = self.__dict__

        # Attributes must support None type
        non_serialized_attributes = [
            'socket',
            'dashboard_task',
            'consumer_task',
        ]
        for attr in non_serialized_attributes:
            if obj.get(attr):
                obj[attr] = None
        return obj

    @property
    async def dashboard(self) -> typing.Optional[discord.Message]:
        """The discord message for a dashboard.

        |coro|

        This is not ideal as in our code, you cannot chain properties that you
        must await.

        A common pattern to use here will be:
        db = await self.dashboard
        await db.edit(...)

        This should probably be a method to avoid confusion but keeping it as is.

        Returns
        -------
        Optional[:class:`discord.Message`]
        """
        if self.dashboard_channel is None:
            return
        if self._dashboard is None:
            return
        try:
            dashboard = await self.dashboard_channel.fetch_message(self._dashboard)
        except discord.errors.NotFound:
            log.debug('Dashboard was deleted')
            return
        return dashboard

    @dashboard.setter
    def dashboard(self, dashboard: typing.Union[int, discord.Message]):
        self._safe_set('_dashboard', dashboard)

    @property
    def category(self) -> typing.Optional[discord.CategoryChannel]:
        if self._category is None:
            return
        try:
            ch = self.guild.get_channel(self._category)
            return ch
        except discord.errors.NotFound:
            return

    @category.setter
    def category(self, channel: typing.Union[int, discord.CategoryChannel]):
        self._safe_set('_category', channel)

    @property
    def lobby_channel(self) -> typing.Optional[discord.VoiceChannel]:
        if self._lobby_channel is None:
            return
        try:
            ch = self.guild.get_channel(self._lobby_channel)
            return ch
        except discord.errors.NotFound:
            return

    @lobby_channel.setter
    def lobby_channel(self, ch: typing.Union[int, discord.VoiceChannel]):
        self._safe_set('_lobby_channel', ch)

    @property
    def streamer_channel(self) -> typing.Optional[discord.VoiceChannel]:
        if self._streamer_channel is None:
            return
        try:
            ch = self.guild.get_channel(self._streamer_channel)
            return ch
        except discord.errors.NotFound:
            return

    @streamer_channel.setter
    def streamer_channel(self, ch: typing.Union[int, discord.VoiceChannel]):
        self._safe_set('_streamer_channel', ch)

    @property
    def team_channels(self) -> typing.List[discord.VoiceChannel]:
        result = [
            discord.utils.get(self.guild.voice_channels, id=id)
            for id in self._team_channels
        ]
        return [o for o in result if o is not None]

    @team_channels.setter
    def team_channels(self, vcs: typing.List[typing.Union[int, discord.VoiceChannel]]):
        self._safe_set('_team_channels', vcs, default=[])

    @property
    def originator(self) -> typing.Optional[discord.Member]:
        return self.guild.get_member(self._originator)

    @originator.setter
    def originator(self, member: typing.Union[int, discord.Member]):
        self._safe_set('_originator', member)

    @property
    async def announcement(self) -> typing.Optional[discord.Message]:
        """The discord message for a channel announcement of lobby status.

        |coro|

        This is not ideal as in our code, you cannot chain properties that you
        must await.

        A common pattern to use here will be:
        a = await self.announcement
        await a.edit(...)

        This should probably be a method to avoid confusion but keeping it as is.

        Returns
        -------
        Optional[:class:`discord.Message`]
        """
        if not self._announcement_channel or not self._announcement:
            return
        try:
            msg = await self.announcement_channel.fetch_message(self._announcement)
            return msg
        except discord.errors.HTTPException:
            self._announcement = None
            return self._announcement


    @announcement.setter
    def announcement(self, message: typing.Union[int, discord.Message]):
        self._safe_set('_announcement', message)

    @property
    async def unlinked_embed(self) -> typing.Optional[discord.Message]:
        """The discord message for an unlinked handle alert.

        |coro|

        This is not ideal as in our code, you cannot chain properties that you
        must await.

        A common pattern to use here will be:
        ue = await self.unlinked_embed
        await ue.edit(...)

        This should probably be a method to avoid confusion but keeping it as is.

        Returns
        -------
        Optional[:class:`discord.Message`]
        """
        if any([self._channel is None, self._unlinked_embed is None]):
            return
        try:
            return await self.dashboard_channel.fetch_message(self._unlinked_embed)
        except discord.errors.NotFound:
            return

    @unlinked_embed.setter
    def unlinked_embed(self, message: typing.Union[int, discord.Message]):
        self._safe_set('_unlinked_embed', message)

    @property
    def command_invoked(self) ->typing.Optional[discord.ext.commands.Command]:
        """Stores a command to the session.

        This is primarily used to recover and invoke a command that the player
        started with. A workflow is like the following:

        Player queues the bot with a specific command relevant to league rules
            - professional
            - intermediate
            - up and coming

        A dashboard displays, and the players check in.

        As a match starts, the dashboard dissapears.

        When a match ends, the same command is re-invoked to display the same
        dashboard.

        Returns
        -------
        Optional[:class:`Command`]
        """
        if isinstance(self._command_invoked, str):
            return client.get_command(self._command_invoked)

    @command_invoked.setter
    def command_invoked(self, cmd: typing.Union[str, discord.ext.commands.Command]):
        self._safe_set('_command_invoked', cmd, default_attr='name', type_req=str)

    @property
    def announcement_channel(self) -> typing.Optional[discord.TextChannel]:
        """Stores the channel that an announcement message was sent in.

        We store channels to be able to easily find messages by id without
        having to iterate through every :class:`TextChannel` on the guild.

        Returns
        -------
        Optional[:class:`TextChannel`]
        """
        return self.guild.get_channel(self._announcement_channel)

    @announcement_channel.setter
    def announcement_channel(self, channel: typing.Union[int, discord.TextChannel]):
        self._safe_set('_announcement_channel', channel)

    @property
    def host_role(self) -> typing.Optional[discord.Role]:
        if self._host_role is None:
            return
        return self.guild.get_role(self._host_role)

    @host_role.setter
    def host_role(self, role: typing.Union[int, discord.Role]):
        self._safe_set('_host_role', role)


    @property
    def announcement_role(self) -> typing.Optional[discord.Role]:
        """Dashboards create custom mentionable roles to give players alerts.

        When a dashboard is created, a new role is created and assigned to any
        player that checks into that lobby. This role will be used to help
        send special alerts throughout the session lifecycle.

        Roles are removed when a session ends.

        Returns
        -------
        Optional[:class:`discord.Role`]
        """
        return self.guild.get_role(self._announcement_role)

    @announcement_role.setter
    def announcement_role(self, role: typing.Union[int, discord.Role]):
        self._safe_set('_announcement_role', role)

    @property
    def last_activity(self) -> datetime.datetime:
        """The last time a checkin was processed on a dashboard.

        This is utilized primarily to check if a session should timeout.

        Returns
        -------
        :class:`datetime.datetime`
        """
        return dateutil.parser.parse(self._last_activity)

    @last_activity.setter
    def last_activity(self, dt: datetime.datetime):
        self._last_activity = dt.isoformat()

    @property
    def guild(self) -> discord.Guild:
        """Retrieves the guild that the HostSession lives on.

        Returns
        -------
        :class:`discord.Guild`
        """
        return client.get_guild(self._guild)

    @property
    def hosts(self) -> typing.List[discord.Member]:
        """Gets a list of members that are the session hosts

        Hosts can send commands and have their commands manipulate a particular
        session instead of starting their own, even if they were not the owner
        of the original session.

        Returns
        -------
        List[:class:`discord.Member`]
        """
        result = [
            discord.utils.get(self.guild.members, id=id)
            for id in self._hosts
        ]
        return [o for o in result if o is not None]

    @hosts.setter
    def hosts(self, hosts: typing.List[typing.Union[int, discord.Member]]):
        self._safe_set('_hosts', hosts, default=[])

    @property
    def dashboard_channel(self):
        """Stores the channel that a dashboard message was sent in.

        We store channels to be able to easily find messages by id without
        having to iterate through every :class:`TextChannel` on the guild.

        Returns
        -------
        Optional[:class:`TextChannel`]
        """
        return client.get_channel(self._channel)

    @dashboard_channel.setter
    def dashboard_channel(self, channel: typing.Union[int, discord.TextChannel]):
        self._safe_set('_channel', channel)

    @property
    def checkins(self) -> typing.List[discord.Member]:
        """
        Get a list of Discord Members that have checked in.

        This will also remove any members that may have left the server during
        this time. Internal state is not managed and should be checked if needed.

        Returns
        -------
        List[:class:`discord.Member`]
        """
        seq = [
            discord.utils.get(self.guild.members, id=id)
            for id in self._checkins
        ]
        return [o for o in seq if o is not None]

    @checkins.setter
    def checkins(self, members: typing.List[typing.Union[int, discord.Member]]):
        self._safe_set('_checkins', members, default=[])


    @property
    def streamer(self) -> typing.Optional[discord.Member]:
        """Represents the person assigned as a streamer in a session

        Under the hood, a streamer is assumed to be the member with an active
        websocket pushing messages to a REDIS channel. Channels on our redis
        server are dependent on the discord id as an identifier.

        Returns
        -------
        :class:`discord.Member`
        """
        member = discord.utils.get(self.guild.members, id=self._streamer)
        return member

    @streamer.setter
    def streamer(self, member: typing.Union[int, discord.Member]):
        self._safe_set('_streamer', member)


    @property
    def rolled_in(self) -> typing.List[discord.Member]:
        """Returns a list of type discord.Member that represents the number of
        times that a Member rolled into a match successfully.
        For example, if the member has been in two games, they will have two
        entries here. This is meant to be used with Collections.Counter

        If a member leaves the server, a None value may exist. This
        function will suppress any None values.

        Returns
        -------
        List[:class:`discord.Member`]
        """
        seq = [
            discord.utils.get(self.guild.members, id=id)
            for id in self._rolled_in
        ]
        return [o for o in seq if o is not None]

    @rolled_in.setter
    def rolled_in(self, members: typing.List[typing.Union[int, discord.Member]]):
        self._safe_set('_rolled_in', members, default=[])

    @property
    def rolled_out(self):
        """Returns a list of type discord.Member that represents the number of
        times that a Member was rolled out of a match and didn't play.
        For example, if the member missed two games, they will have two
        entries here. This is meant to be used with Collections.Counter

        If a member leaves the server, a None value may exist. This
        function will suppress any None values.

        Returns
        -------
        List[:class:`discord.Member`]
        """
        seq = [
            discord.utils.get(self.guild.members, id=id)
            for id in self._rolled_out
        ]
        return [o for o in seq if o is not None]

    @rolled_out.setter
    def rolled_out(self, members: typing.List[typing.Union[int, discord.Member]]):
        self._safe_set('_rolled_out', members, default=[])

    @property
    def required_roles(self) -> typing.List[discord.Role]:
        """A dashboard can restrict those that check in by checking if they have
        any number of roles assigned to them.

        The role check passes if ANY of these roles are assigned.

        Returns
        -------
        List[:class:`discord.Role`]
        """
        seq = [
            discord.utils.get(self.guild.roles, id=id)
            for id in self._required_roles
        ]
        return [o for o in seq if o is not None]

    @required_roles.setter
    def required_roles(self, roles: typing.List[discord.Role]):
        self._safe_set('_required_roles', roles, default=[])

    def is_originator(self, member: discord.Member):
        originator = self.originator
        if isinstance(originator, discord.Member):
            return member.id == originator.id
        return False

    async def delete_stateful(self, o: typing.Any):
        if isinstance(o, list):
            for item in o:
                await self.delete_stateful(item)
            return
        try:
            await o.delete()
        except AttributeError:
            pass
        except discord.errors.NotFound as e:
            log.warning(f"Object not found: {e}")
        except discord.errors.HTTPException as e:
            log.warning(f"HTTP Exception: {e}")

    async def send_data(self, data):
        """
        Sends data over the websocket
        Parameters
        ----------
        data

        Returns
        -------

        """"""
        """
        if self.socket and self.socket.open:
            log.debug("Sending data from host session")
            await self.socket.send(data)

    async def end(self, fallback_channel: typing.Optional[discord.VoiceChannel]=None):
        """
        Ends the session and removes all objects
        Returns
        -------

        """
        stateful_objects = [
            'announcement_role', 'team_channels', 'streamer_channel',
            'lobby_channel', 'category', 'host_role',
        ]

        db = await self.dashboard
        announcement = await self.announcement
        unlinked_embed = await self.unlinked_embed
        await utils.delete_message(db)
        await utils.delete_message(announcement)
        await utils.delete_message(unlinked_embed)

        if isinstance(fallback_channel, discord.VoiceChannel):
            for ch in self.team_channels:
                for m in ch.members:
                    await utils.move_to_voice(m, fallback_channel, reason="Session End")
            if isinstance(self.lobby_channel, discord.VoiceChannel):
                for m in self.lobby_channel.members:
                    await utils.move_to_voice(m, fallback_channel, reason="Session End")
            if isinstance(self.streamer_channel, discord.VoiceChannel):
                for m in self.streamer_channel.members:
                    await utils.move_to_voice(m, fallback_channel, reason="Session End")

        for attr in stateful_objects:
            if not hasattr(self, attr):
                continue
            obj = self.__getattribute__(attr)
            if isinstance(obj, list):
                await self.delete_stateful(obj)
                setattr(self, attr, [])
            else:
                await self.delete_stateful(obj)
                setattr(self, attr, None)

        self.stop_tasks()
        if self.socket and self.socket.open:
            await self.socket.close()




    async def start_tasks(self):
        name = self.originator.display_name
        log.debug(f"Starting tasks on {name}'s HostSession")
        self.last_activity = utils.utc_now()
        self.consumer_task = client.loop.create_task(self.websocket_consumer())
        self.consumer_task.add_done_callback(self.consumer_callback)
        self.dashboard_task = client.loop.create_task(self.update_dashboard())
        self.dashboard_task.add_done_callback(self.dashboard_callback)
        log.debug("Task start complete")

    async def open_streamer_connection(self, streamer:discord.Member = None):
        streamer = streamer if streamer is not None else self.streamer
        if streamer is None:
            log.error("Attempting to open websocket without streamer set")
            return

        if self.socket is not None and self.socket.open:
            await self.socket.close()

        host = os.environ.get('WEBSOCKET_HOST', 'ws://localhost:8000')
        uri = f"{host}/ws/obs/{streamer.id}/"
        log.debug(f"Socket connecting to {uri}")
        self.socket = await websockets.connect(uri)
        result = "Succeeded" if self.socket.open else "Failed"
        log.info(f"Establishing Socket Connection {result}")
        self.streamer = streamer

    async def create_channels(self, *, position=None):

        options = {}
        if isinstance(position, int):
            options['position'] = position

        guild = self.guild
        host = self.originator.display_name
        cat_name = f"{host}'s Game"
        contrib_role = await utils.get_or_create_role(self.guild, 'Contributors')
        admin_role = await utils.get_or_create_role(self.guild, 'Admin')
        admin_overrides = discord.PermissionOverwrite(
            manage_channels=True,
            manage_roles=True,
        )
        try:
            self.host_role = await guild.create_role(
                name=f"{host}'s Game Hosts",
                mentionable=True,
                reason="New Game Lobby Created"
            )
            self.category = await guild.create_category(cat_name)
            if self.originator and self.originator.voice:
                try:
                    pos = self.originator.voice.channel.category.position
                    await self.category.edit(position=pos)
                except AttributeError:
                    # No category. Move to top
                    await self.category.edit(position=0)
            else:
                await self.category.edit(position=0)

            streamer_overwrites = {
                guild.default_role: discord.PermissionOverwrite(connect=False),
                self.host_role: discord.PermissionOverwrite(
                    connect=True,
                    move_members=True,
                    mute_members=True,
                ),
                contrib_role: admin_overrides,
                admin_role: admin_overrides,
            }
            self.lobby_channel = (
                await guild.create_voice_channel(
                    "Lobby",
                    category=self.category,
                    overwrites={
                        self.host_role: discord.PermissionOverwrite(
                            move_members=True,
                            mute_members=True,
                        ),
                        contrib_role: admin_overrides,
                        admin_role: admin_overrides,
                    }
                )
            )
            self.streamer_channel = (
                await guild.create_voice_channel(
                    "Streamers",
                    overwrites=streamer_overwrites,
                    category=self.category
                )
            )
        except discord.errors.Forbidden:
            log.error("Requires manage_channels, manage_roles")

    def start_consumer_task(self):
        """Creates a new consumer task on the HostSession.
        This is seperated into its own function to facilitate recovery from
        a disconnection.

        Returns
        -------
        None
        """
        pass
        # self.websocket_consumer.start()

    def start_dashboard_task(self):
        """Creates a new dashboard task on the HostSession.
        This is seperated into its own function to facilitate recovery from
        a disconnection.

        This task is responsible for keeping a dashboard updated with the
        most relevant information based on a certain interval.

        Returns
        -------
        None
        """
        pass
        # self.update_dashboard.start()


    async def websocket_consumer(self):
        """A task that consumes a socket when opened and dispatches events

        The REDIS server will always return a Dict[str, Any] where a main
        event will have a 'type' key. This is mainly the event from a game that
        we are listening in on.

        We are not using Tasks module here as it checks globally for the task.
        We want task per instance. This should be fixed in discord.py 1.3

        Returns
        -------
        None
        """

        while True:
            if self.socket is None or self.socket is not None and not self.socket.open:
                log.warning("Stream connection closed. Attempting Establish...")
                if self.streamer is None:
                    self.streamer = self.originator
                await self.open_streamer_connection(self.streamer)


            if self.socket is not None and self.socket.open:
                try:
                    log.debug('listening for next event')
                    event = await self.socket.recv()
                    event = json.loads(event)
                    event_type = event.get('type')
                    if event_type:
                        client.dispatch(f"ws_event_{event_type}", self, event)
                except json.decoder.JSONDecodeError:
                    log.error(f"Websocket received non JSON Response {event}")
                except websockets.exceptions.ConnectionClosed:
                    log.error(f"Websocket closed connection while listening")
                    if self.streamer:
                        log.warning("Attempting to recover socket connection")
                        await self.open_streamer_connection()
                    client.dispatch('ws_consumer_task_error', self)
            await asyncio.sleep(1)

    # @websocket_consumer.before_loop
    # async def before_websocket_consumer(self):
    #     """Checks to see if a valid streamer is already set. This makes
    #     recovery from a disconnection more streamlined.
    #
    #     Returns
    #     -------
    #     None
    #     """
    #     if not self.streamer:
    #         return
    #     if self.socket is None or self.socket is not None and not self.socket.open:
    #         await self.open_streamer_connection()



    async def update_dashboard(self):
        """Background task that will check for dashboard, and update it.

        We use a task to avoid POSTING too much for when people rapidly check
        in and out. Some players spam the reaction button that is mainly used
        to facilitate this.

        Returns
        -------
        None
        """
        while True:
            dashboard = await self.dashboard if self._dashboard else None
            now = utils.utc_now()
            last_activity = self.last_activity
            delta = now - self.last_activity
            if dashboard:
                # log.debug(f"Delta {delta}")
                if delta > datetime.timedelta(hours=4, minutes=30):
                    log.warning(f'Detected Elapsed time: Now: {now} Last: {last_activity} Delta {delta}')
                    if self.dashboard_task:
                        self.dashboard_task.cancel()
                    client.dispatch("session_timeout", self)
                try:
                    await dashboard.edit(embed=self.generate_dashboard())
                except discord.errors.NotFound:
                    self._checkins = []
                    self._dashboard = None
                    log.warning(f"Queue Embed was deleted. Resetting session. {self}")
                    continue
                except discord.errors.Forbidden:
                    log.error("Missing Manage Channel Permissions")
                    continue
                except:
                    raise
            await asyncio.sleep(2)

    def consumer_callback(self, task: asyncio.Task):
        """
        Returns
        -------
        None
        """
        try:
            if task.exception():
                log.error(task.print_stack())
                log.error("Websocket Consumer crashed to exception.")
                self.consumer_task = client.loop.create_task(self.websocket_consumer())
                self.consumer_task.add_done_callback(self.consumer_callback)

        except asyncio.futures.CancelledError:
            pass
    # @update_dashboard.after_loop
    def dashboard_callback(self, task: asyncio.Task):
        """ If the task ends, more than likely it was cancelled. The only use
        case at this moment is that the session met the last_activity timeout.
        Returns
        -------
        None
        """
        try:
            if task.exception():
                log.debug(task.print_stack())
            client.dispatch('session_timeout', session=self)
        except asyncio.futures.CancelledError:
            pass

    """
    Sorting and processing
    """
    async def ping_announcement(self):
        """When a lobby reaches its minimum players needed, it sends a ping
        notification and announcement to a channel alerting players that the
        lobby is ready.

        Returns
        -------
        None
        """
        if self._pinged_roles:
            return
        author = self.originator
        role = self.announcement_role
        announcement = await self.announcement
        dashboard = await self.dashboard
        await utils.delete_message(announcement)
        description = textwrap.dedent(
            f"""
            {role.mention} We have enough players to start!
            Make sure to join on {author.mention}'s lobby in SC2.
            And don't forget to join the VC!
            
            
            [Click here for the dashboard!]({dashboard.jump_url})
            """
        )
        embed = utils.BrandedEmbed(
            title=f"{author.display_name}'s Game is Ready!",
            description=description
        )
        ch = self.announcement_channel
        await ch.send(f"{role.mention}", delete_after=180)
        self.announcement = await ch.send(embed=embed)
        self._pinged_roles = True

    async def create_dashboard(self, ch:discord.TextChannel, announcement_ch = None):
        """Creates a new dashboard in a specified channel.

        Dashboards only exist one at a time, so this will remove any existing
        dashboard and the announcements that accompany it.

        Parameters
        ----------
        ch: :class:`discord.TextChannel`
            The channel to create the dashboard in.
        announcement_ch: Optional[:class:`discord.TextChannel`]
            A different channel to send announcements, if any.

        Returns
        -------
        None
        """
        author = self.originator
        db = await self.dashboard
        announcement = await self.announcement
        await utils.delete_message(db)
        await utils.delete_message(announcement)
        self.announcement_role = await utils.get_or_create_role(
            guild=self.guild,
            name=f"{author.display_name}'s Lobby",
            mentionable=True
        )
        self._pinged_roles = False
        self.dashboard_channel = ch
        self.last_activity = utils.utc_now()
        dashboard = await ch.send(embed=self.generate_dashboard())
        await dashboard.add_reaction(EMOJIS['CHECKIN'])
        self.dashboard = dashboard

        if announcement_ch is not None and isinstance(announcement_ch, discord.TextChannel):

            announcement = textwrap.dedent(
                f"""
                {author.mention} has hosted a new game for checkin.
                You must click the PLAY button on their dashboard to check-in.


                [Click here for the dashboard!]({dashboard.jump_url})


                (Dashboard located in {ch.mention})
                """
            )
            embed = utils.BrandedEmbed(
                title="NEW GAME AVAILABLE",
                description=announcement
            )
            self.announcement_channel = announcement_ch
            self.announcement = await announcement_ch.send(embed=embed)

        return dashboard

    def is_checked_in(self, member: typing.Union[int, discord.Member]) -> bool:
        """Checks if a member is checked in to the HostSession

        Parameters
        ----------
        member: Union[int, :class:`discord.Member`]
            The member to check

        Returns
        -------
        bool
        """
        member = safe_value(member)
        return member in self._checkins

    def checkin_player(self, member: typing.Union[int, discord.Member]):
        """Checks a member into the HostSession

        Parameters
        ----------
        member: Union[int, :class:`discord.Member`]
            The member to check in.

        Returns
        -------
        None
        """
        id = safe_value(member)
        self._checkins.append(id)

    def checkout_player(self, member: typing.Union[int, discord.Member]):
        """Checks a member out from the HostSession

        Parameters
        ----------
        member: Union[int, :class:`discord.Member`]
            The member to check out.

        Returns
        -------
        None
        """
        id = safe_value(member)
        self._checkins = [i for i in self._checkins if i != id]


    def player_priority(self) -> typing.List[discord.Member]:
        """Returns an ordered list of members by their priority.

        The lower the index on the list, the higher the priority.

        This method also enables functionality for priortizing different cases,
        such as if winners should advance or not.

        Returns
        -------
        List[:class:`discord.Member`]
        """
        with_games = self._rolled_in
        rolled_out = self._rolled_out
        no_games = [(id, 0) for id in self._checkins if id not in with_games]
        # Get counts of re-occurances
        with_games = collections.Counter(with_games).most_common()
        rolled_out = dict(collections.Counter(rolled_out).most_common())
        adjusted = with_games
        # Has there been anyone thats rolled out? If so let's adjust their sort index
        if rolled_out:
            if with_games:
                adjusted = [(discord_id, num-rolled_out.get(discord_id, 0)) for discord_id, num in with_games]
            if no_games:
                no_games = [(discord_id, num-rolled_out.get(discord_id, 0)) for discord_id, num in no_games]
        # Re-convert back to Member objects
        adjusted = [
            (discord.utils.get(self.guild.members, id=discord_id), num)
            for discord_id, num in adjusted
        ]
        no_games = [
            (discord.utils.get(self.guild.members, id=discord_id), num)
            for discord_id, num in no_games
        ]

        # Remove any None type values? I think this could only happen if someone checked in and
        # then left the server.
        adjusted = [o for o in adjusted if o[0] is not None]
        no_games = [o for o in no_games if o[0] is not None]
        result = no_games + adjusted
        result = [p for p in result if p[0] in self.checkins]
        if self.winners_advance and len(self.matches) > 0:
            try:
                last_match_id = max(self.matches.keys())
                winners = self.matches[last_match_id].get('winners', [])
                winners = [
                    discord.utils.get(self.guild.members, id=w.get('discord_id'))
                    for w in winners
                ]
                winners = [w for w in winners if w is not None and w in self.checkins]

                # Get the winners out of the result that has the index. This assumes a
                # player has properly checked in.
                result = [(p, num) for p, num in result if p not in winners]
                result = winners + [p for p, num in sorted(result, key=lambda o: o[1], reverse=False)]
                return result
            except KeyError:
                result = [r[0] for r in sorted(result, key=lambda o: o[1], reverse=False)]
                return result
        else:
            result = [r[0] for r in sorted(result, key=lambda o: o[1], reverse=False)]
            return result

    async def process_checkin(self, emoji:discord.Emoji, user: discord.Member,
                              *,checkin=True) -> None:
        """Helper function to process a checkin, or a checkout and track state.

        This does the necessary role restriction checks and also checks if a
        game has reached its minimum amount.
        Parameters
        ----------
        emoji: :class:`discord.Emoji`
            The emoji that was clicked.
        user: :class:`discord.Member`
            The member to check in or check out.
        checkin: bool
            True to check-in, False to check out.

        Returns
        -------
        None
        """
        minimum_players = 8
        koth_role = await utils.get_or_create_role(
            self.guild,
            name='King of the Hill',
            mentionable=True
        )
        if user == client.user:
            return
        if emoji.__str__() != EMOJIS['CHECKIN']:
            return
        self.last_activity = utils.utc_now()
        if checkin:
            if not self.required_roles or any(r in user.roles for r in self.required_roles + [koth_role]):
                self.checkin_player(user)
                if self.announcement_role:
                    await user.add_roles(self.announcement_role, reason="New Game Checkin")
                client.dispatch('match_checkin', user=user, session=self)
                if len(self._checkins) >= minimum_players:
                    await self.ping_announcement()
            else:
                await self.remove_player_reaction(user)
                await self.dashboard_channel.send(
                    f"{user.display_name} does not meet required roles.",
                    delete_after=5
                )
        else:
            self.checkout_player(user)
            if self.announcement_role:
                await user.remove_roles(self.announcement_role, reason="New Game Checkout")
            client.dispatch('match_checkout', user=user, session=self)


    async def remove_player_reaction(self, player: discord.Member):
        """Helper function to remove a player reaction.

        Parameters
        ----------
        player: :class:`discord.Member`
            The player to remove the reaction from.

        Returns
        -------
        None
        """
        dashboard = await self.dashboard
        if dashboard:
            await dashboard.remove_reaction(EMOJIS['CHECKIN'], player)

    """
    Embeds
    """
    def generate_unlinked_embed(self) -> utils.BrandedEmbed:
        """An embed that displays unlinked profiles in a session.

        Unlinked profiles are captured from the bot by the 'player_info' key
        which will have a None object for the 'discord_id' field. These will get
        appended to unlinked_roles in duplicate fashion much like roles in. Once
        linked it will be merged with the rolled_in list object.

        Returns
        -------
        :class:`utils.BrandedEmbed`
        """
        title = "Warning, Unlinked Profiles Detected!!"
        description = "Use ,(q)ueue (l)ink <name> @DiscordMember\n\n"
        unique_unlinked = self.unlinked_profiles
        try:
            unique_unlinked = [json.dumps(d, sort_keys=True) for d in unique_unlinked]
            unique_unlinked = list(set(unique_unlinked))
            unique_unlinked = [json.loads(d) for d in unique_unlinked]
        except json.JSONDecodeError:
            unique_unlinked = self.unlinked_profiles
        for player in unique_unlinked:
            try:
                description += f"{player.get('name')}\n"
            except KeyError:
                pass
        embed = utils.BrandedEmbed(title=title, description=description)
        return embed

    def generate_dashboard(self, *args, **kwargs) -> utils.BrandedEmbed:
        """An embed that displays the dashboard and check-in information.

        Shows the settings for the current session so players can choose
        if they want to check-in to this session or not.

        Parameters
        ----------
        args
        kwargs

        Returns
        -------
        :class:`utils.BrandedEmbed`
        """
        hosts = []
        originator = self.originator
        if originator is not None:
            hosts.append(originator)
        else:
            log.error(f"Originator was set to None.")

        hosts += self.hosts
        title = kwargs.get('title') or "Queue Manager"
        description = kwargs.get('description') or "Check in below"
        embed = utils.BrandedEmbed(title=title, description=description, dashboard=True)
        hosts = ', '.join(p.display_name for p in hosts) or "No Hosts"
        required_roles = ', '.join(r.name for r in self.required_roles) or "No Restrictions"
        winners_advance = "Yes" if self.winners_advance else "No"
        priorities = self.player_priority()
        priority = ', '.join(p.display_name for p in priorities[:8]) or "\u200b"
        alternates = ', '.join(p.display_name for p in priorities[8:]) or "\u200b"
        streamer_id = str(self.streamer.id) if self.streamer else "No Streamer"
        embed.add_field(name="Hosts", value=hosts, inline=False)
        embed.add_field(name="Streamer", value=self.streamer or "No Streamer")
        embed.add_field(name="Streamer ID", value=streamer_id)
        embed.add_field(name="Twitch Stream", value=self.twitch_stream)
        embed.add_field(name="Membership Restriction", value=required_roles)
        embed.add_field(name="Winners Advance?", value=winners_advance)
        embed.add_field(name=f"Prioritized - {len(priorities[:8])}", value=priority, inline=False)
        embed.add_field(name=f"Alternates - {len(priorities[8:])}", value=alternates, inline=False)
        embed.set_footer(text="To check in (or out of) this match, click the 'play' button.")
        return embed


