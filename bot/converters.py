import logging
from collections import Sequence
from functools import partial

import discord
from discord.ext import commands
from discord.ext.commands import Context

from bot import utils
from bot.webapi import APIObjectDoesNotExist
from bot.webapi import WebAPIMixin

log = logging.getLogger(__name__)


async def convert_argument(f, arg, attr=None):
    if isinstance(arg, Sequence) and not isinstance(arg, str):
        if attr:
            return [await f(getattr(o, attr)) for o in arg]
        return [await f(o) for o in arg]
    else:
        if attr:
            return await f(getattr(arg, attr))
        return await f(arg)


class MessageConverter(commands.Converter):
    async def convert(self, ctx: Context, argument):
        if not argument.isdigit():
            raise commands.BadArgument("Message ID must be integer")
        ch:discord.TextChannel = ctx.channel
        msg = await ch.fetch_message(int(argument))
        if msg:
            return msg
        else:
           raise commands.BadArgument("Could not locate message")



class TimeDelta(commands.Converter):
    async def convert(self, ctx, argument):
        """
        Format is
        1w2d1h18m2s

        :param last_active:
        :return:
        """
        td = utils.twitch_to_time_delta(argument)
        if td is None:
            raise commands.BadArgument(f"Invalid TimeDelta {argument}")
        return td


class GameType(commands.Converter):
    async def convert(self, ctx, argument):
        """Converter to parse text and make sure it is a valid game type listed
        below. This will raise BadArgument if the text is not any one of the
        following game_types
        """
        game_types = [
            '1v1', '1v1v1', '1v1v1v1',
            '2v2', '2v2v2', '2v2v2v2',
            '3v3', '4v4'
        ]
        if argument not in game_types:
            raise commands.BadArgument(f"Invalid Game Type: {argument}")
        return argument


class TeamNum(commands.Converter):
    async def convert(self, ctx, argument):
        """Ensures that the TeamNum given is an integer between 1 and 4.
        """
        if int(argument) not in [1,2,3,4]:
            raise commands.BadArgument(f"Valid teams 1-4. Got {argument}")
        return argument


class LeagueAPIObject(WebAPIMixin, commands.Converter):
    async def convert(self, ctx, argument):
        try:
            argument = int(argument)
        except ValueError:
            raise commands.BadArgument(f"{argument} must be an integer.")
        league = await self.get_record('league', argument)
        if not league:
            raise APIObjectDoesNotExist(f"{self.__class__} - id {argument} not found.")
        return league


class DiscordPlayerAPIObject(WebAPIMixin, commands.Converter):
    async def convert(self, ctx, argument):
        discord_member = partial(commands.MemberConverter().convert, ctx)
        site_member = partial(self.get_record, 'discordplayer')
        argument = await convert_argument(discord_member, argument)
        argument = await convert_argument(site_member, argument, attr='id')
        return argument

class EventAPIObject(WebAPIMixin, commands.Converter):
    async def convert(self, ctx, argument):
        try:
            argument = int(argument)
        except ValueError:
            raise commands.BadArgument(f"{argument} must be an integer.")
        event = await self.get_record('event', argument)
        if not event:
            raise APIObjectDoesNotExist(f"{self.__class__} - id {argument} not found.")
        return event

class MatchAPIObject(WebAPIMixin, commands.Converter):
    async def convert(self, ctx, argument):
        try:
            argument = int(argument)
        except ValueError:
            raise commands.BadArgument(f"{argument} must be an integer.")
        event = await self.get_record('match', argument)
        if not event:
            raise APIObjectDoesNotExist(f"{self.__class__} - id {argument} not found.")
        return event

class TournamentAPIObject(WebAPIMixin, commands.Converter):
    async def convert(self, ctx, argument):
        try:
            argument = int(argument)
        except ValueError:
            raise commands.BadArgument(f"{argument} must be an integer.")
        tournament = await self.get_record('tournament', argument)
        if not tournament:
            raise APIObjectDoesNotExist(f"{self.__class__} - id {argument} not found.")
        return tournament