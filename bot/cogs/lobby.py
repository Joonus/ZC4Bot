import asyncio
import logging
import typing
from copy import deepcopy
from string import Template

import aiohttp
import discord
from dateutil.parser import parse as parse_date
from discord.ext import commands

from bot.utils import BrandedEmbed
from bot import utils

log = logging.getLogger(__name__)
from bot import constants
import datetime


class DeltaTemplate(Template):
    delimiter = "%"

def strfdelta(tdelta, fmt):
    d = {"D": tdelta.days}
    hours, rem = divmod(tdelta.seconds, 3600)
    minutes, seconds = divmod(rem, 60)
    d["H"] = '{:02d}'.format(hours)
    d["M"] = '{:02d}'.format(minutes)
    d["S"] = '{:02d}'.format(seconds)
    t = DeltaTemplate(fmt)
    return t.substitute(**d)


class LobbyViewer(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.loop = asyncio.get_event_loop()
        self.bot = bot
        self.data = []
        self.api_url = 'http://sc2arcade.talv.space/api/open-games'
        self.lobbies = {}
        self.retry_seconds = 0
        self.embed_maps: typing.Dict[str, int] = {}
        self.status_embed: typing.Optional = None
        self.channel: typing.Optional[discord.TextChannel] = None
        self.poll_task: typing.Optional[asyncio.Task] = None


    def make_status_embed(self):
        next_try = datetime.datetime.utcnow() + datetime.timedelta(seconds=self.retry_seconds)
        embed = utils.BrandedEmbed(title="Lobby Announcer Status")
        embed.add_field(name="Status", value="**DOWN**")
        embed.timestamp = next_try
        embed.set_footer(text="Next retry at ")
        return embed


    async def poll(self):

        while True:
            try:
                async with aiohttp.ClientSession() as session:
                    data = await session.get(self.api_url)
                    self.data = await data.json()
                    await self.update(self.data)
                if self.poll_task.done() and not self.poll_task.cancelled():
                    print(self.poll_task.exception())

                # No exception, status is UP
                self.retry_seconds = 0
                if self.status_embed is not None:
                    await utils.delete_message(self.status_embed)
                    self.status_embed = None
                await asyncio.sleep(5)
            except Exception as e:
                log.debug(e)
                # Just wipe everything
                self.embed_maps = {}
                self.data = []
                self.lobbies = {}
                await self.update_channel_name()
                self.retry_seconds += 30
                if self.status_embed is None:
                    await self.channel.purge(limit=100)
                    self.status_embed = await self.channel.send(embed=self.make_status_embed())
                else:
                    await self.status_embed.edit(embed=self.make_status_embed())
                await asyncio.sleep(self.retry_seconds)





    def poll_callback(self, task: asyncio.Task):

        try:
            if task.exception():
                log.debug(task.print_stack())

        except asyncio.futures.CancelledError:
            pass

    def get_lobby_id(self, lobby: typing.Dict[str, str]):

        id = "{0}{1}".format(lobby['bnetBucketId'], lobby['bnetRecordId'])
        return id

    async def fetch_message(self, message_id: int) -> typing.Optional[discord.Message]:
        try:
            message: discord.Message = await self.channel.fetch_message(message_id)
            return message
        except (discord.NotFound, discord.Forbidden, discord.HTTPException):
            return

    async def remove_message(self, message_id: int):
        message = await self.fetch_message(message_id)
        if message is not None:
            await message.delete()

    async def get_lobby_message(self, lobby_id: str) -> typing.Optional[discord.Message]:
        message_id = self.embed_maps.get(lobby_id)
        if message_id is None:
            return
        message = await self.fetch_message(message_id)
        return message

    async def remove_invalid_lobbies(self):
        iter_obj = deepcopy(self.embed_maps)
        for lobby_id, message_id in iter_obj.items():
            # If the lobby no longer exists
            ids = list(self.lobbies.keys())
            if lobby_id not in ids:
                await self.remove_message(message_id)
                del self.embed_maps[lobby_id]

    def get_players(self, lobby) -> typing.Sequence[typing.Tuple[str, str, str, str]]:
        # Order, Name, Flair

        created = parse_date(lobby['createdAt'])

        container = []
        players = [p for p in lobby['players'] if p['leftAt'] is None]
        for n, p in enumerate(players):
            joined = parse_date(p['joinedAt'])
            elapsed = joined - created
            elapsed = strfdelta(elapsed, '%M:%S')

            seq = str(n+1)
            name = p['name']
            flair = "**Host**" if name == lobby['hostName'] else ""

            item = (seq, elapsed, name, flair)
            container.append(item)
        return container

    def make_embed(self, lobby) -> BrandedEmbed:
        description = lobby.get('lobbyTitle', "")
        region = lobby['region']['code']
        mode = lobby['mapVariantMode']
        embed = BrandedEmbed(title="Zone Control CE", description=description)
        players = '\n'.join(f"**{i}.** `+{t}` {name} {flair}" for i, t, name, flair in self.get_players(lobby))
        embed.add_field(name="Players", value=players)
        embed.timestamp = parse_date(lobby['createdAt'])
        embed.set_footer(text=f"Mode: {mode} | Region: {region} ")
        return embed

    async def update_channel_name(self):
        name = self.channel.name.split('-')

        new = len(self.lobbies)
        try:
            n = int(name[0])
            if n != new:
                self.channel: discord.TextChannel
                await self.channel.edit(name=f"{new}-open-lobbies")
        except (ValueError, discord.Forbidden):
            await self.channel.edit(name=f"{new}-open-lobbies")

    async def update(self, data: typing.Sequence[typing.Dict[str, str]]):
        # Filter Zone Control
        game = 'Zone Control CE'
        data = [ce for ce in data if ce['mapDocumentVersion']['document']['name'] == game]
        self.lobbies = {self.get_lobby_id(lobby): lobby for lobby in data}

        await self.remove_invalid_lobbies()
        await self.update_channel_name()

        for id, lobby in self.lobbies.items():
            lobby_msg = await self.get_lobby_message(id)
            if lobby_msg is None:
                if self.channel is not None:
                    message = await self.channel.send(embed=self.make_embed(lobby))
                    self.embed_maps[id] = message.id
            else:
                await lobby_msg.edit(embed=self.make_embed(lobby))

    def start_polling(self):
        self.poll_task = self.loop.create_task(self.poll())
        self.poll_task.add_done_callback(self.poll_callback)

    @commands.Cog.listener()
    async def on_ready(self):
        self.channel = self.bot.get_channel(679569093007114266)
        await self.channel.purge(limit=100)
        self.start_polling()

    @commands.command(name='lobby')
    @commands.has_any_role(*constants.ROLES['MODERATORS'])
    async def lobby(self, ctx):
        await ctx.send("Restarting Lobby Announcer", delete_after=5)
        if self.poll_task:
            self.poll_task.cancel()

        self.start_polling()



def setup(bot: commands.Bot):
    bot.add_cog(LobbyViewer(bot))
    log.info("Cog loaded: Lobby Announcer")
