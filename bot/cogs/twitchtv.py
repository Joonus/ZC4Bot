import asyncio
import datetime
import logging
import os
from collections import defaultdict
from typing import Dict, Any, List, Optional

import aiohttp
import discord
from discord.ext import commands, tasks
from discord.ext.commands import Bot, Context, Cog
from discord.utils import get

from bot import restapi
from bot import utils
import pickle
import textwrap
from bot import constants
import json



log = logging.getLogger(__name__)
UPDATE_DELAY = 120
STREAM_ROLE_NAME = 'Streamer'

FILE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..', 'static'))
PATH = os.path.normpath(f'{FILE_DIR}/twitchtv.json')

class TwitchTV(Cog):

    def __init__(self, bot: Bot):
        self.bot = bot
        self.config = defaultdict(dict)
        self.data = []
        self.messages = {}


    def cog_unload(self):
        pass

    def persist_config(self):
        with open(PATH, 'w') as f:
            json.dump(self.config, f)

    @Cog.listener()
    async def on_stream_start(self, guild: int, payload):
        log.debug(payload)
        guild: discord.Guild = discord.utils.get(self.bot.guilds, id=guild)
        channel: discord.TextChannel = discord.utils.get(guild.text_channels, name="general")
        user_id = int(payload['user_id'])
        member: discord.Member = discord.utils.get(guild.members,id=user_id)
        stream_name = payload['data'][0]['title']
        user_name = payload['data'][0]['user_name']
        description = f""" Stream started for {member.display_name}\n
                Click here to view:\n
                [{stream_name}](https://twitch.tv/{user_name})"""
        embed = discord.Embed(title="Stream Starting", description=description,colour=discord.Colour.blue())
        thumbnail = payload['data'][0]['thumbnail_url']
        embed.set_thumbnail(url=thumbnail.format(width=64, height=64))

        message: discord.Message = await channel.send(embed=embed)
        self.messages[str(user_id)] = message.id

    @Cog.listener()
    async def on_stream_stop(self, guild: int, payload):
        guild: discord.Guild = discord.utils.get(self.bot.guilds, id=guild)
        channel: discord.TextChannel = discord.utils.get(guild.text_channels, name="general")
        user_id = int(payload['user_id'])
        message_id = self.messages.get(str(user_id))
        try:
            message: discord.Message = await channel.fetch_message(message_id)
            await message.delete()
        except (discord.Forbidden,discord.HTTPException,discord.NotFound):
            log.warning('Message no longer exists')
        del self.messages[str(user_id)]







    @Cog.listener()
    async def on_new_match_stream(self, guild: discord.Guild, payload):
        log.debug('received new_match stream')
        log.debug(type(payload))
        log.debug(payload)

        embed = utils.BrandedEmbed(
            title="Stream Alert"
        )
        players = [p['name'] for p in payload['players']]
        streamer_value = ""
        thumb = payload['streamers'][0]['stream']['extra_data']['data'][0]['thumbnail_url'].format(width=64,height=64)
        for p in payload['streamers']:
            stream_name = p['stream']['extra_data']['data'][0]['title']
            viewers = p['stream']['extra_data']['data'][0]['viewer_count']
            text = f"[{p['profile']['name']}/{stream_name}](https://twitch.tv/{p['stream']['username']}) - {viewers} watching\n"
            streamer_value += text
        embed.add_field(name='Players', value=', '.join(players) or 'No Players', inline=False)
        embed.add_field(name='Streamers', value=streamer_value, inline=False)
        embed.set_thumbnail(url=thumb)
        embed.timestamp = datetime.datetime.utcnow()
        embed.set_footer(text=f"{len(payload['streamers'])} streaming...")
        guild = self.bot.guilds[0]
        ch = discord.utils.get(guild.channels, name='general')
        await ch.send(embed=embed)



    @Cog.listener()
    async def on_member_update(self, before_user: discord.Member,
                               after_user: discord.Member):
        """
        Tracks when a member updates their status and looks to see if they
        started "Streaming". Add a specific role to them to call them to the
        top of the discord list.
        :param before_user: The Member state before the update happened
        :param after_user: The Member state after the update happened
        :return: None
        """
        stream_role = get(after_user.guild.roles or [], name=STREAM_ROLE_NAME)
        if not stream_role:
            return

        if any(isinstance(a, discord.Streaming) for a in after_user.activities):
            log.debug(f"{after_user.display_name} Started Streaming")
            if stream_role not in after_user.roles:
                await after_user.add_roles(stream_role,
                                           reason="Started Streaming Activity",
                                           atomic=True)
        else:
            if stream_role in after_user.roles:
                await after_user.remove_roles(stream_role,
                                              reason="Ended Streaming Activity",
                                              atomic=True)

    @Cog.listener()
    async def on_command_completion(self, ctx: Context):
        if ctx.cog != self:
            return
        await utils.reaction(True, ctx)
        await asyncio.sleep(5)
        await utils.delete_message(ctx.message)

    async def cog_command_error(self, ctx: Context, error: discord.ext.commands.CommandError):
        if ctx.cog != self:
            return
        log.error(error)
        error.with_traceback(error.__traceback__)
        await utils.reaction(False, ctx)
        await asyncio.sleep(5)
        await utils.delete_message(ctx.message)
        raise error


def setup(bot: Bot):
    bot.add_cog(TwitchTV(bot))
    log.info("Cog loaded: TwitchTV")




