from discord.ext import commands, tasks
import discord
import asyncio
import collections
import typing
import os
from bot import restapi
import io
import base64
import urllib.parse
from bot.cogs.translate import Translate
import logging

class VoiceItem:

    def __init__(self, ch: discord.VoiceChannel, contents: str, **kwargs):
        self.channel = ch
        self.contents = contents
        self.kwargs: typing.Dict[str, typing.Any] = kwargs


_uri = 'https://texttospeech.googleapis.com/'
_api_key = os.environ.get('GOOGLE_API_TOKEN')
assert _api_key is not None

log = logging.getLogger(__name__)

def tts_uri(target: str, **kwargs) -> str:
    parts = list(urllib.parse.urlparse(_uri))
    parts[2] = f"v1/{target}"
    kwargs['key'] = _api_key
    parts[4] = urllib.parse.urlencode(kwargs)
    return urllib.parse.urlunparse(parts)

class GoogleTTS(commands.Cog):
    """
    Uses Google TTS service to provide better TTS support in functions.
    """

    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.queue = asyncio.Queue()
        self.ready = asyncio.Event()

        self.ready.set()
        self.tts_task.start()

    def cog_unload(self):
        self.tts_task.cancel()


    @tasks.loop()
    async def tts_task(self):
        await self.ready.wait()
        item:VoiceItem = await self.queue.get()
        self.ready.clear()

        language = await self.get_voice(item)
        item.kwargs.update(language)
        pcm = await self.synthesize(item, **item.kwargs)
        vcl = await item.channel.connect()
        vcl.play(pcm, after=lambda e: self.disconnect(vcl))

    def disconnect(self, voice_client: discord.VoiceClient):
        coro = voice_client.disconnect()
        future = asyncio.run_coroutine_threadsafe(coro, self.bot.loop)
        future.result()
        self.queue.task_done()
        self.bot.loop.call_soon_threadsafe(self.ready.set)

    async def speak(self, vc: discord.VoiceChannel, msg: str, **kwargs):
        """
        Uses bot to talk!
        Parameters
        ----------
        vc
        msg
        kwargs

        Returns
        -------

        """
        item = VoiceItem(vc, msg, **kwargs)
        await self.queue.put(item)

    async def get_voice(self, item: VoiceItem) -> typing.Dict[str, str]:
        """
        Attempts to use a translate cog to get a dialect
        Returns
        -------

        """
        result = {
            'languageCode': 'en-US',
            'name': 'en-US-Wavenet-F',
        }
        translate: Translate = self.bot.get_cog('Translate')
        if not translate:
            return result
        language = await translate.detect_language(item.contents)
        uri = tts_uri('voices', languageCode=language)
        _, resp = await restapi.api_method('get', target=uri)
        voices = resp.get('voices')
        if not voices:
            return result
        choices = sorted(
            [l for l in voices if l['ssmlGender'] == 'FEMALE'],
            key=lambda k: k['name'],
            reverse=True
        )[0]
        result['languageCode'] = choices['languageCodes'][0]
        result['name'] = choices['name']
        return result

    async def synthesize(self, item: VoiceItem, **kwargs
                         ) -> typing.Optional[discord.PCMAudio]:
        uri = tts_uri('text:synthesize')
        payload = {
            "audioConfig": kwargs.get('audioConfig') or {
                "audioEncoding": kwargs.get('audioEncoding') or "LINEAR16",
                "sampleRateHertz": kwargs.get('sampleRateHertz') or 96000,
                "speakingRate": kwargs.get('speakingRate') or 1,
            },
            "input": kwargs.get('input') or {
            },
            "voice": kwargs.get('voice') or {
                "languageCode": kwargs.get('languageCode') or 'en-US',
                "name": kwargs.get('name') or 'en-US-Wavenet-F'
            }
        }
        if not payload.get('input', {}):
            input = 'ssml' if kwargs.get('ssml') else 'text'
            payload['input'][input] = (
                    kwargs.get('ssml') or
                    kwargs.get('text') or
                    item.contents
            )

        _, resp = await restapi.api_method('post', target=uri, json=payload)
        data = resp.get('audioContent')
        if not data:
            return
        stream = io.BytesIO(base64.b64decode(data))
        return discord.PCMAudio(stream)

    @commands.command(name='voice', aliases=('v',))
    async def tts_voice(self, ctx, channel: discord.VoiceChannel, *, msg: str):
        await self.queue.put(
            VoiceItem(channel, msg)
        )

    @commands.command(name='voice-translate', aliases=('vtr',))
    async def tts_voice_translate(self, ctx, lang: str, ch: discord.VoiceChannel, *, msg: str):
        translate: Translate = self.bot.get_cog('Translate')
        if not translate:
            return
        translation = await translate.translator.translate(msg, dest=lang)
        await self.queue.put(
            VoiceItem(ch, translation.text)
        )
def setup(bot):
    """Responsible for loading the GoogleTTSCOG extension into discord.py"""
    bot.add_cog(GoogleTTS(bot))
    log.info("Cog loaded: GoogleTTS")