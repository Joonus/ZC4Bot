from discord.ext.commands import command, Context, Bot, Cog, group
import logging
import websockets
import typing
import discord
from bot import utils
import json
import asyncio

log = logging.getLogger(__name__)

class Notifications(Cog):
    def __init__(self, bot: Bot):
        """ZCL Notification Service"""
        self.bot: Bot = bot
        self.loop = asyncio.get_event_loop()
        self._socket: typing.Optional[websockets.WebSocketClientProtocol] = None
        self._task: asyncio.Task = self.loop.create_task(self._listen())
        self._task.add_done_callback(self._listener_finished)
        self.guild: None

    @Cog.listener()
    async def on_ready(self):
        self.guild = self.bot.guilds[0]

    async def get_socket(self) -> websockets.WebSocketClientProtocol:
        url = 'ws://zclbeta.com/notifications/'
        #url = 'ws://zclweb.herokuapp.com/notifications/'
        if self._socket is None or not self._socket.open:
            log.debug("Opening connection to Notification channel")
            self._socket = await websockets.connect(url)
        return self._socket

    def _listener_finished(self, task: asyncio.Task):
        """
        Returns
        -------
        None
        """
        try:
            if task.exception():
                log.error(task.print_stack())

                log.error("Websocket Consumer crashed to exception.")
                asyncio.ensure_future(asyncio.sleep(30))

                self._task = self.loop.create_task(self._listen())
                self._task.add_done_callback(self._listener_finished)


        except asyncio.futures.CancelledError:
            pass

    async def _listen(self):
        """
        Listens to the websocket stream for ZCL Notifications
        :return:
        """
        while True:
            socket = await self.get_socket()
            data = await socket.recv()
            data = json.loads(data)
            for k in data:
                print(k, type(data[k]))

            log.debug(f'Data received: {data}')
            # Get the payload if any
            payload = data.get('payload')
            guild = 476518371320397834
            #guild = discord.utils.get(self.bot.guilds, id=payload['guild'])
            log.debug(f"sending dispatch on_{data['action']}")
            self.bot.dispatch(data['action'], guild, payload)


    def __del__(self):
        if self._socket is not None and self._socket.open:
            self._socket.close()

def setup(bot: Bot):
    bot.add_cog(Notifications(bot))
    log.info("Cog loaded: ZCL Notfications")